import 'package:flutter/material.dart';
import 'package:social_network/ui/address_search.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.dark(), 
      home: AddressSearch());
  }
}
