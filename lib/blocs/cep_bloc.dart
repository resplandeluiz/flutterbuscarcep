import 'package:rxdart/rxdart.dart';
import 'package:social_network/models/cep_model.dart';
import 'package:social_network/resources/repository.dart';

class CepBloc {
  final _repository = Repository();
  final _cepFetcher = PublishSubject<CepModel>();
  final _loading = PublishSubject<bool>();

  Stream<CepModel> get ultimoEnderecoObtido => _cepFetcher.stream;
  Stream<bool> get loading => _loading.stream;

  obterEndereco({required String porCep}) async {
    if (porCep != null && porCep.length == 8) {
      _loading.sink.add(true);
      final enderecoObtido = await _repository.obterEndereco(porCep);
      _loading.sink.add(false);
      _cepFetcher.sink.add(enderecoObtido);
    }

    await _cepFetcher.drain();
  }

  dispose() {
    _loading.close();
    _cepFetcher.close();
  }
}
