import 'package:flutter/material.dart';
import 'package:social_network/blocs/cep_bloc.dart';
import 'package:social_network/models/cep_model.dart';

class AddressSearch extends StatelessWidget {
  final _bloc = CepBloc();
  final _textFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    _textFieldController.addListener(() {
      _bloc.obterEndereco(porCep: _textFieldController.text);
    });

    return Scaffold(
      appBar: AppBar(
        title: Text('Consulta CEP'),
      ),
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              TextFormField(
                controller: _textFieldController,
                decoration: InputDecoration(
                    labelText: 'Digite o CEP para encontrar o endereço',
                    hintText: '00000000',
                    helperText: 'somente números'),
                keyboardType: TextInputType.number,
              ),
              Container(
                child: StreamBuilder(
                  stream: _bloc.ultimoEnderecoObtido,
                  builder: (context, AsyncSnapshot<CepModel> snapshot) {
                    if (snapshot.hasData) {
                      final endereco = snapshot.data!;
                      return Column(
                        children: [
                          Text('CEP: ${endereco.cep}'),
                          Text('UF: ${endereco.uf}'),
                          Text('Cidade: ${endereco.localidade}'),
                          Text('Bairro: ${endereco.bairro}'),
                          Text('Logradouro: ${endereco.logradouro}'),
                        ],
                      );
                    }

                    if (snapshot.hasError) {
                      return Text("Ocorreu um erro!\n${snapshot.error}");
                    }

                    return StreamBuilder(
                      stream: _bloc.loading,
                      builder: (context, AsyncSnapshot<bool> snapshot) {
                        final loading = snapshot.data ?? false;
                        if (loading) {
                          return Center(child: CircularProgressIndicator());
                        }
                        return Container();
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
