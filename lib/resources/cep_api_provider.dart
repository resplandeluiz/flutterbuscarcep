import 'dart:convert';
import 'dart:developer';

import 'package:http/http.dart';
import 'package:social_network/models/cep_model.dart';

class CepApiProvider {
  final client = Client();
  final baseUrl = 'https://viacep.com.br/ws/';

  Future<CepModel> obterEndereco(String cep) async {
    final response =
        await client.get(Uri.parse('$baseUrl$cep/json/'));

    if (response.statusCode == 200) {
      final body = response.body;
      log('Chegou o JSON $body');
      final decodedBody = json.decode(body);
      return CepModel.fromJson(decodedBody);
    } else {
      throw Exception(
          'Erro ao recuperar endereço. Status ${response.statusCode}');
    }
  }
}
