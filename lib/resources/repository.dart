import 'package:social_network/models/cep_model.dart';
import 'package:social_network/resources/cep_api_provider.dart';

class Repository {
  final cepApiProvider = CepApiProvider();

  Future<CepModel> obterEndereco(String cep) =>
      cepApiProvider.obterEndereco(cep);
}
